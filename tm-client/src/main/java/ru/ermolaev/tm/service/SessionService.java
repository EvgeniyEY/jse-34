package ru.ermolaev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import ru.ermolaev.tm.api.ISessionService;
import ru.ermolaev.tm.endpoint.SessionDTO;

@Service
public class SessionService implements ISessionService {

    @Nullable
    private SessionDTO currentSession;

    @Nullable
    @Override
    public SessionDTO getCurrentSession() {
        return currentSession;
    }

    @Override
    public void setCurrentSession(@NotNull final SessionDTO currentSession) {
        this.currentSession = currentSession;
    }

    @Override
    public void clearCurrentSession() {
        currentSession = null;
    }

}
