# PROJECT INFO

TASK MANAGER

# DEVELOPER INFO

**NAME**: EVGENIY ERMOLAEV

**E-MAIL**: ermolaev.evgeniy.96@yandex.ru

# SOFTWARE

- JDK 1.8

- Windows 10

# PROGRAM BUILD

```bash
mvn clean install
```

# DOCKER COMMANDS
### RUN CLUSTER
```bash
docker-compose up -d
```
### SHUTDOWN CLUSTER
```bash
docker-compose down
```

# SCREENSHOTS

https://yadi.sk/d/0VNR3BcQnI6g6g?w=1