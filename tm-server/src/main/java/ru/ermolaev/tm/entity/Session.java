package ru.ermolaev.tm.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "app_session")
public class Session extends AbstractEntity {

    @Nullable
    @Column(columnDefinition = "TEXT",
            updatable = false)
    private Long startTime;

    @Nullable
    @Column(columnDefinition = "TEXT",
            updatable = false)
    private String signature;

    @Nullable
    @ManyToOne
    private User user;

}
