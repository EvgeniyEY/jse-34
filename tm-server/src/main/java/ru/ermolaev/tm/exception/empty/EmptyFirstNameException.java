package ru.ermolaev.tm.exception.empty;

import ru.ermolaev.tm.exception.AbstractException;

public final class EmptyFirstNameException extends AbstractException {

    public EmptyFirstNameException() {
        super("Error! First name is empty.");
    }

}
