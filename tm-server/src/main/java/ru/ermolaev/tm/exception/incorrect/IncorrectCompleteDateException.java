package ru.ermolaev.tm.exception.incorrect;

import org.jetbrains.annotations.NotNull;
import ru.ermolaev.tm.exception.AbstractException;

import java.util.Date;

public final class IncorrectCompleteDateException extends AbstractException {

    public IncorrectCompleteDateException() {
        super("Error! Complete date is incorrect.");
    }

    public IncorrectCompleteDateException(@NotNull final Date date) {
        super("Error! This date [" + date + "] is incorrect.");
    }

}
