package ru.ermolaev.tm.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import ru.ermolaev.tm.api.endpoint.IAdminDataEndpoint;
import ru.ermolaev.tm.api.service.IBackupService;
import ru.ermolaev.tm.api.service.ISessionService;
import ru.ermolaev.tm.dto.SessionDTO;
import ru.ermolaev.tm.enumeration.Role;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
@Controller
@NoArgsConstructor
public final class AdminDataEndpoint implements IAdminDataEndpoint {

    private ISessionService sessionService;

    private IBackupService backupService;

    @Autowired
    public AdminDataEndpoint(
            @NotNull final ISessionService sessionService,
            @NotNull final IBackupService backupService
    ) {
        this.sessionService = sessionService;
        this.backupService = backupService;
    }

    @Override
    @WebMethod
    public void saveXmlByJaxb(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO
    ) throws Exception {
        sessionService.validate(sessionDTO, Role.ADMIN);
        backupService.saveXmlByJaxb();
    }

    @Override
    @WebMethod
    public void loadXmlByJaxb(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO
    ) throws Exception {
        sessionService.validate(sessionDTO, Role.ADMIN);
        backupService.loadXmlByJaxb();
    }

    @Override
    @WebMethod
    public void clearXmlFileJaxb(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO
    ) throws Exception {
        sessionService.validate(sessionDTO, Role.ADMIN);
        backupService.clearXmlFileJaxb();
    }

    @Override
    @WebMethod
    public void saveXmlByFasterXml(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO
    ) throws Exception {
        sessionService.validate(sessionDTO, Role.ADMIN);
        backupService.saveXmlByFasterXml();
    }

    @Override
    @WebMethod
    public void loadXmlByFasterXml(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO
    ) throws Exception {
        sessionService.validate(sessionDTO, Role.ADMIN);
        backupService.loadXmlByFasterXml();
    }

    @Override
    @WebMethod
    public void clearXmlFileFasterXml(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO
    ) throws Exception {
        sessionService.validate(sessionDTO, Role.ADMIN);
        backupService.clearXmlFileFasterXml();
    }

    @Override
    @WebMethod
    public void saveJsonByJaxb(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO
    ) throws Exception {
        sessionService.validate(sessionDTO, Role.ADMIN);
        backupService.saveJsonByJaxb();
    }

    @Override
    @WebMethod
    public void loadJsonByJaxb(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO
    ) throws Exception {
        sessionService.validate(sessionDTO, Role.ADMIN);
        backupService.loadJsonByJaxb();
    }

    @Override
    @WebMethod
    public void clearJsonFileJaxb(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO
    ) throws Exception {
        sessionService.validate(sessionDTO, Role.ADMIN);
        backupService.clearJsonFileJaxb();
    }

    @Override
    @WebMethod
    public void saveJsonByFasterXml(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO
    ) throws Exception {
        sessionService.validate(sessionDTO, Role.ADMIN);
        backupService.saveJsonByFasterXml();
    }

    @Override
    @WebMethod
    public void loadJsonByFasterXml(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO
    ) throws Exception {
        sessionService.validate(sessionDTO, Role.ADMIN);
        backupService.loadJsonByFasterXml();
    }

    @Override
    @WebMethod
    public void clearJsonFileFasterXml(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO
    ) throws Exception {
        sessionService.validate(sessionDTO, Role.ADMIN);
        backupService.clearJsonFileFasterXml();
    }

    @Override
    @WebMethod
    public void saveBinary(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO
    ) throws Exception {
        sessionService.validate(sessionDTO, Role.ADMIN);
        backupService.saveBinary();
    }

    @Override
    @WebMethod
    public void loadBinary(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO
    ) throws Exception {
        sessionService.validate(sessionDTO, Role.ADMIN);
        backupService.loadBinary();
    }

    @Override
    @WebMethod
    public void clearBinaryFile(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO
    ) throws Exception {
        sessionService.validate(sessionDTO, Role.ADMIN);
        backupService.clearBinaryFile();
    }

    @Override
    @WebMethod
    public void saveBase64(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO
    ) throws Exception {
        sessionService.validate(sessionDTO, Role.ADMIN);
        backupService.saveBase64();
    }

    @Override
    @WebMethod
    public void loadBase64(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO
    ) throws Exception {
        sessionService.validate(sessionDTO, Role.ADMIN);
        backupService.loadBase64();
    }

    @Override
    @WebMethod
    public void clearBase64File(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO
    ) throws Exception {
        sessionService.validate(sessionDTO, Role.ADMIN);
        backupService.clearBase64File();
    }

}
